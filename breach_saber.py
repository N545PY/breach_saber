#!/usr/bin/env python3

import argparse
import sqlite3

def search(db_in, str_in, number, args):
    con = sqlite3.connect(db_in)
    cur  = con.cursor()
    for x in args:
        if x == "email":
            data = cur.execute("SELECT * FROM breachdb WHERE EMAIL = '?';", (str(str_in)))
        else:
            data = cur.execute("SELECT * FROM breachdb WHERE PASSWORD = '?';", (str(str_in)))

    return data.fetchmany(number)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--database", help="db to connect to")
    parser.add_argument("-n", "--number", help="number of results to fetch",
                        default=50)
    parser.add_argument("-e", "--email", help="email to search")
    parser.add_argument("-p", "--password", help="password to search")
    args = parser.parse_args()
    if len(args.email) != 0:
        print(search(args.database, args.email, args.number, "email"))
    else:
        search(args.database, args.password, args.number, "pass")
