#!/usr/bin/env python3

import os
import sys
import csv
import argparse
import json as j


def file_gen(file_in):
    with open(file_in, "r", errors="ignore") as file1:
        for line in file1:
            yield line

def mask_line(str_in):
    tmp_mask = []
    for char in list(str_in):
        if char.islower():
            tmp_mask.append("?l")
        elif char.isupper():
            tmp_mask.append("?u")
        elif char.isdigit():
            tmp_mask.append("?d")
        elif char.isalnum() is not None:
            tmp_mask.append("?s")
    mask_str = ''.join(tmp_mask)
    return mask_str
def bad_line(str_in, bad_outfile):
    with open(bad_outfile, "a") as bad_out:
        bad_out.write(str_in)


def dict_build(str_in, mask):
    if len(str_in) != 0:
        line_dict = {}
        try:
            email, password = str_in.split(":")
            domain = email.split("@")[1]
            line_dict["username"] = str(email)
            line_dict["password"] = password.rstrip()
            line_dict["domain"] = domain.rstrip()
            if mask:
                line_dict["mask"] = mask_line(password.rstrip())
                line_dict["password-length"] = len(password)
        except IndexError as e:
            pass
        except ValueError as e:
            pass

        return line_dict


def parse_file_csv(file_in, csv_out, mask):
    with open(file_in, "r", encoding="UTF-8", errors='ignore') as in_file:
        with open(csv_out, "w") as csv_out:
            fieldnames = ["username", "password", "domain", "mask", "password-length"]
            writer = csv.DictWriter(csv_out, fieldnames=fieldnames)
            writer.writeheader()
            print(f"starting {file_in}")
            for line in in_file:
                if len(line) != 0:
                    try:
                        leak_dict = dict_build(line, mask)
                        writer.writerow(leak_dict)
                    except UnicodeDecodeError as uni_error:
                        print(uni_error)


def parse_file_ndjson(file_in, file_out, mask):
    lines = 0
    with open(file_out, "w") as f:
        for line in file_gen(file_in):
            if len(line) != 0:
                try:
                    lines +=1
                    f.write(j.dumps(dict_build(line, mask)) + "\n")
                except:
                    pass




def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--in_file", help="File to import")
    parser.add_argument("-o", "--out", help="File output")
    parser.add_argument("-b", "--bad_out", help="File to place bad lines in",
                        default="bad.out")
    parser.add_argument("-j", "--json", help="Parse file to json",
                        action="store_true")
    parser.add_argument("-c", "--csv", help="Parse file to csv", default=True,
                        action="store_true")
    parser.add_argument("-m", "--masker", help="Genrate Password masks",
                        action="store_true", default=False)
    args = parser.parse_args()

    if args.json:
        parse_file_ndjson(args.in_file, args.out, args.masker)
    else:
        parse_file_csv(args.in_file, args.out, args.masker)

if __name__ == "__main__":
    main()
