#!/usr/bin/env python3

import argparse
import sqlite3
import masker
import tqdm

#  Setups database

def init_db(file_out):
    try:
        con = sqlite3.connect(file_out)
        cur = con.cursor()
        cur.execute('''CREATE TABLE IF NOT EXISTS
                    breachdb(ID INTEGER NOT NULL PRIMARY KEY,
                     EMAIL TEXT, PASSWORD TEXT, DOMAIN TEXT, USERNAME TEXT)''')
        cur.execute('''CREATE INDEX email_pass_idx ON breachdb(
                    EMAIL, PASSWORD)''')
        cur.commit()
    except:
        pass


# dict builder used for indexing
# Takes a string input, mask is a bool

def dict_build(str_in):
    if len(str_in) != 0:
        line_dict = {}
        try:
            email, password = str_in.split(":")
            domain = email.split("@")[1]
            line_dict["username"] = str(email)
            line_dict["password"] = password.rstrip()
            line_dict["domain"] = domain.rstrip()
        except IndexError as e:
            pass
        except ValueError as e:
            pass

        return line_dict

# inserts a buffer (list, set by args.size). reads in buffer.
# After it is done the db is commited.


def insert_emails(lst_in, db_path, prog, cl):
    con = sqlite3.connect(db_path)
    cur = con.cursor()
    i = 0
    for x in lst_in:
        try:
            cur.execute('''INSERT INTO breachdb(EMAIL, PASSWORD, DOMAIN)
            VALUES(?,?,?);''', (x["username"], x["password"], x["domain"]))
            i += 1
        except:
            continue
    con.commit()
    prog.update(i)
    if cl:
        con.close()

# main funct to parse files
# works with a list buffer that is set by "batch_size"
# Prog is a obect for the progress bar, it is passed to insert_emails


def index_file(file_in, db_path, batch_size, prog):
    email_list = []
    l = []
    id_num = 0
    with open(file_in, "r", errors="ignore") as f:
        for line in f:
            if len(line) != 0:
                id_num += 1
                email_list.append(dict_build(line))
                if len(email_list) == int(batch_size):
                    insert_emails(email_list, db_path, prog, False)
                    email_list = []
    insert_emails(l, db_path, prog, True)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--database", help="path to sqlite3 file")
    parser.add_argument("-i", "--input_file", help="file to ingest into sqlite3")
    parser.add_argument("-s", "--size", help="How many lines to que")
    parser.add_argument("-v", "--verbose", action="store_true")
    args = parser.parse_args()
    print("Counting lines, might take a while...")
    lines = 0
    with open(args.input_file, "r", errors="ignore") as f:
        for line in f:
            lines += 1
    print(f"Indexing {lines} lines")
    progress = tqdm.tqdm(unit="lines", total=lines)
    init_db(args.database)
    index_file(args.input_file, args.database, args.size, progress)

main()
