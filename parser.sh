#!/usr/bin/env bash
set -euo pipefail


function setup {
    wd = $PWD
    if [ !-d leaks ]; then
        mkdir -vp leaks
    fi

    if [ ! -d db ]; then
        mkdir -vp db
    fi
    if [ ! -d clean ]; then
        mkdir -vp clean
    fi
}

function sort_files {
    total_files=$(ls *.txt | wc -l)
    echo "sorting $total_files files"
        for d_file in *.txt; do
            echo "removing null bytes in: $d_file..."
            sed -i 's/\\x00//g' $d_file
            echo "Deleting lines that do not contain emails..."
            grep  "@" $d_file > $d_file.tmp
            echo "Making sure seperator exists in: $d_file..."
            sed -i 's/;/:/' $d_file.tmp && sed -i 's/ /:/' $d_file.tmp
            sort $d_file.tmp | uniq > clean/$d_file
            echo "sorted $(ls clean/* | wc -l) out of $total_files"
        done
echo "Done"
}




function file_parser {
    for file in clean/*.txt; do
        out=$(echo "$file" | cut -d "/" -f 2 | cut -d "." -f 1)
        case $1 in
            json)
                echo "parsing $out in $1 format $(ls db/* | wc -l )/$total_files"
                python masker.py -j -i $file -o ./db/$out.json
                ;;
            csv)
                python masker.py -c -i $file -o ./db/$out.csv
                ;;
            sqlite3)
                python index_sqlite.py -d ./db/$2.db -i $file -s 1500
                ;;
        esac
    done
}
setup
sort_files
file_parser $1
case $1 in
    json|csv)
        file_parser $1
        ;;
    sqlite3)
        file_parser $1 $2
        ;;
esac
echo "Exiting"
